import request from 'request';
import { createObjectCsvWriter as createCsvWriter } from 'csv-writer';
import { v4 as uuidv4 } from 'uuid';

let ACCESS_TOKEN = 'pk.eyJ1IjoiYWRyaWFub2N0IiwiYSI6ImNsNndoa3J6bTBqOHQzanBnczJnNWMyc3IifQ.Qk5sufyEiMqw52fugkyTzQ';

const forwardGeocoding = function (address) {
    let url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/'
        + encodeURIComponent(address) + '.json?access_token='
        + ACCESS_TOKEN + '&limit=1';

    request({ url: url, json: true }, function (error, response) {
        if (error) {
            callback('Unable to connect to Geocode API', undefined);
        } else if (response.body.features.length == 0) {
            callback('Unable to find location. Try to '
                + 'search another location.');
        } else {

            var longitude = response.body.features[0].center[0];
            var latitude = response.body.features[0].center[1];
            var location = response.body.features[0].place_name;

            console.log("Latitude:", latitude);
            console.log("Longitude:", longitude);
            console.log("Location:", location);

            const csvWriter = createCsvWriter({
                append: true,
                path: 'places.csv',
                header: [
                    { id: 'Id_gerado', title: 'Id gerado:' },
                    { id: 'Latitude', title: 'Latitude' },
                    { id: 'Longitude', title: 'Longitude' },
                    { id: 'Location', title: 'Location' }
                ]
            });

            let data = [
                {
                    Id_gerado: uuidv4(),
                    Latitude: latitude,
                    Longitude: longitude,
                    Location: location
                }
            ]

            csvWriter
                .writeRecords(data)
                .then(() => console.log('The CSV file was written successfully!'));

        }
    })
}

// Coloque aqui o nome do lugar que deseja obter as coordenadas geográficas!
var address = 'Ouro Preto';

// Chamada da função!
forwardGeocoding(address);