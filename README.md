# Como funciona o programa:

- Dentro de index.js, na linha 57, há uma variável de nome address.
- Troque o nome da String para um lugar que deseja.
- O resultado será armazenado dentro de places.csv.
- Será informado, nessa ordem: Id (randômico), Latitude, Longitude, Localização.

# Requisitos:
- npm i -s csv-parser
- npm i -s csv-writer
- npm i -s fast-csv
- npm install uuid